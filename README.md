# For information about the questions please go to the challenge.md file

## Below is an explanation about the code used to solve the challenge

## The code in stats.py contains two functions: add_entry_to_stats and merge_stats.

**add_entry_to_stats**

The add_entry_to_stats function takes two dictionaries as inputs, stats and entry, and returns a dictionary that combines the information from both.

It starts by making a deep copy of the stats dictionary to ensure that the input data is not modified. Then, it checks if the stats dictionary is empty. If it is, the function initializes it with values for count, amount, and num_items.

Next, the function checks if the entry dictionary is empty. If it is, the function returns the stats dictionary.

The function then adds information from entry to stats. If the values of amount and num_items in the entry dictionary are not of type int or float, a TypeError is raised.

The function then calculates the new values for count, amount, and num_items. If there are any other keys in the entry dictionary, the function checks if the key is already in the stats dictionary. If it is, the function updates the values for count, amount, and num_items for the corresponding key. If the key is not in the stats dictionary, the function adds it with the values for count, amount, and num_items taken from entry.

**merge_stats**

The merge_stats function takes one or more dictionaries as inputs and returns a dictionary that combines the information from all of the input dictionaries.

The function first checks if the inputs are all dictionaries. If not, a TypeError is raised. If the function has no inputs, a ValueError is raised. If the function has only one input, it returns the input.

The function then takes the first input dictionary as the starting point and adds the information from the rest of the dictionaries to it. If the dictionaries have common keys, the function merges the values for these keys recursively. If the values are numeric (int or float), they are added together. If the values are strings, they are concatenated. If the values are dictionaries, the function calls itself recursively to merge the dictionaries. If the values are of different types, a TypeError is raised.

## Additional tests

## The code in tests_additional.py contains two classes TestTask1_Additional and TestTask2_Additional that perform unit tests on two functions add_entry_to_stats and merge_stats.

**TestTask1_Additional**
This class contains two tests for the function add_entry_to_stats.
* The first test test_add_empty_entry tests the behavior of the function when an empty entry is passed. It checks if the function returns the same statistics as the input.
* The second test test_deep_copy tests the behavior of the function when an entry with values is passed. It also checks if the function returns a deep copy of the input statistics and not a shallow copy.

**TestTask2_Additional**
This class contains two tests for the function merge_stats.

* The first test test_empty_merge tests the behavior of the function when an empty dictionary is passed. It checks if the function returns the same statistics as the input.
* The second test test_string_type_merge tests the behavior of the function when multiple non-empty dictionaries are passed. It checks if the function returns a merged dictionary of all the inputs.

