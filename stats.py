import copy 


def add_entry_to_stats(stats: dict, entry: dict) -> dict:
    # Copy the enter dict to not modify the 
    # input dict
    # ret_val = stats.copy()
    ret_val = copy.deepcopy(stats)

    # Treating the case when the stats dict
    # is empty (i.e. we are initializing the dict)
    if len(ret_val) == 0:
        ret_val = {
            'count': 0,
            'amount': 0,
            'num_items': 0,
        }

   # Checking if entry dict is empty
    if len(entry) == 0:
        return ret_val

    # General common information
    amount = entry['amount']
    num_items = entry['num_items']

    # Checking amount and num_items types
    if not isinstance(entry['amount'], (int, float)) and not isinstance(entry['num_items'], (int, float)):
        raise TypeError("Amount and num_items must be int or float.")

    # Calculates the new general information
    # I assume this information always exist in the stats 
    # dict
    ret_val['count'] += 1
    ret_val['amount'] += amount
    ret_val['num_items'] += num_items

    # Entries common to all objects
    common_entries = ['amount', 'num_items']
    
    # Getting the keys of the entry dict
    entries = entry.keys()
    # Getting just the ones that are not 'amount' or 'num_items'
    entries = [item for item in entries if item not in common_entries]

    # If there is nothing left, I just return the resulting dict
    if len(entries) == 0:
        return ret_val
    
    # Now, I iterate over the other entries
    for item in entries:
        # If it is already in the dict
        if item in stats.keys():
            # I check if its value is already in the dict
            entry_item = entry[item]
            # If not, just initialize its value
            if entry_item not in ret_val[item].keys():
                ret_val[item][entry_item] = {
                    'count': 0,
                    'amount': 0,
                    'num_items': 0,
                } 
            # Then update its value   
            ret_val[item][entry_item]['count'] += 1
            ret_val[item][entry_item]['amount'] += amount
            ret_val[item][entry_item]['num_items'] += num_items
        # If it is not in the dict, I just create its entry
        else:
            entry_item = entry[item]
            ret_val[item] = {
                entry_item: {
                    'count': 1,
                    'amount': amount,
                    'num_items': num_items
                }
            }


    return ret_val

def merge_stats(*args: dict) -> dict:
    # TODO: Check if type hint throws errors
    for arg in args:
        if type(arg) != dict:
            raise TypeError("Not supported type. Please, enter just dicts.")

    # Making sure that the function has arguments
    if len(args) == 0:
        raise ValueError("There is no data do merge")
    # If the function receives just one element,
    # it returns the given element
    if len(args) == 1:
        return copy.deepcopy(args[0])
    # Now we deal with two or more elements
    # We start getting the first entry as the 
    # our beginning and will merge the other entries
    # to it
    ret_val = copy.deepcopy(args[0])
    for arg in args[1:]: # Beginning from the 2nd entry
        stat = copy.deepcopy(arg)
        # First, I get the keys from both dicts
        left_keys = ret_val.keys()
        right_keys = stat.keys()
        # Now I get the common keys
        common_keys = [key for key in left_keys if key in right_keys]
        # And the keys ONLY in the right dict. I don't need to
        # worry about the left dict, since the data will be appended
        # to it
        right_only_keys = [key for key in right_keys if key not in common_keys]
        # Now I append only the right_only_keys to the left dict
        for key in right_only_keys:
            ret_val[key] = stat[key]
        # Now, for the common keys, we implement a recursive
        # feature to accept nested objects of any depth
        for key in common_keys:
            # If the objects are numeric, just add them to the
            # left dict
            if (type(ret_val[key]) in [int, float] and
                type(stat[key] in [int, float])):
                ret_val[key] += stat[key]
                # If the objects are both string
            elif (type(ret_val[key]) == str and 
                type(stat[key]) == str):
                ret_val[key] = ret_val[key] + '.' + stat[key]
            # If the objects are dicts, recursive calling
            # the function until we get to the numeric objects
            elif (type(ret_val[key]) == dict and
                  type(stat[key]) == dict):
                  ret_val[key] = merge_stats(ret_val[key], stat[key])
            # Otherwise, I throw an error
            else:
                raise TypeError("Cannot add non-numeric to numeric value'")

            
    return ret_val
